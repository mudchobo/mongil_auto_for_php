<?php

function check_new_dungeon($img, $osInfo, $area, $stage)
{
    // 최하단이 아니면 맨 최하단좌측으로 이동.
    if (checkPixel($img, 163, 722, 13678486, 'map left bottom'))
    {

        if ($area == '1')
        {
            if ($stage == '1' || $stage == '2' || $stage == '3' || $stage == '4')
            {
                drag(300, 400, 20, 400);
                sleep(2);
            }
            else
            {
                drag(300, 400, 20, 400);
                sleep(2);
                drag(400, 400, 10, 400);
                sleep(2);
            }
            if ($stage == '1')
            {
                clickEventForGenymotion(25, 276);
            }
            else if ($stage == '2')
            {
                clickEventForGenymotion(210, 400);
            }
            else if ($stage == '3')
            {
                clickEventForGenymotion(259, 663);
            }
            else if ($stage == '4')
            {
                clickEventForGenymotion(370, 540);
            }
            else if ($stage == '5')
            {
                clickEventForGenymotion(164, 527);
            }
            else if ($stage == '6')
            {
                clickEventForGenymotion(313, 438);
            }
        }
        else if ($area == '2')
        {
            drag(420, 200, 1, 200);
            sleep(2);
            drag(420, 200, 1, 200);
            sleep(2);
            drag(420, 200, 1, 200);
            sleep(2);
            if ($stage == '1')
            {
                clickEventForGenymotion(26, 504);
            }
            else if ($stage == '2')
            {
                clickEventForGenymotion(122, 388);
            }
            else if ($stage == '3')
            {
                clickEventForGenymotion(194, 242);
            }
            else if ($stage == '4')
            {
                clickEventForGenymotion(320, 219);
            }
            else if ($stage == '5')
            {
                clickEventForGenymotion(409, 325);
            }
            else if ($stage == '6')
            {
                clickEventForGenymotion(363, 481);
            }
            else if ($stage == '7')
            {
                clickEventForGenymotion(209, 512);
            }
            else if ($stage == '8')
            {
                clickEventForGenymotion(97, 655);
            }
        }
        else if ($area == '3')
        {
            drag(420, 200, 1, 200);
            sleep(2);
            drag(420, 200, 1, 200);
            sleep(2);
            drag(200, 600, 200, 1);
            sleep(2);
            if ($stage == '1')
            {
                clickEventForGenymotion(365, 174);
            }
            else if ($stage == '2')
            {
                clickEventForGenymotion(310, 316);
            }
            else if ($stage == '3')
            {
                clickEventForGenymotion(317, 464);
            }
            else if ($stage == '4')
            {
                clickEventForGenymotion(213, 381);
            }
            else if ($stage == '5')
            {
                clickEventForGenymotion(85, 294);
            }
            else if ($stage == '6')
            {
                clickEventForGenymotion(95, 463);
            }
            else if ($stage == '7')
            {
                clickEventForGenymotion(118, 621);
            }
            else if ($stage == '8')
            {
                clickEventForGenymotion(196, 744);
            }
        }
    }
    else
    {
        drag(1, 90, 400, 700);
        drag(1, 90, 400, 700);
        drag(1, 90, 400, 700);
    }
}
