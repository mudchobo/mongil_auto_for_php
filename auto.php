<?php

if (count($argv) <= 2)
{
    echo 'argv입력하삼.';
    exit;
}
$isAutoSkill = $argv[1] == 'true' ? true : false;
$dungeon = $argv[2];
echo 'isAutoSkill = ' . $isAutoSkill . ', dungeon = ' . $dungeon . PHP_EOL;

while(true)
{
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen.png");
    $img = imagecreatefrompng('screen.png');

    checkClick($img, 177, 372, 16423542, 16423542, '게임실행');
    checkClick($img, 61, 1709, 6512489, 6512489, '광고팝업닫기');
    checkClick($img, 122, 915, 5908488, 5908488, '공지팝업');
    checkClick($img, 903, 1504, 10793513, 10465320, '화면을 터치해주세요');
    checkClick($img, 180, 822, 5906440, 6038282, '친구도우미이력 확인');
    checkClick($img, 156, 687, 14595683, 14135899, '구매팝업닫기');
    checkClick($img, 74, 1164, 9221821, 9352896, '모험입장');

    // 별지역인 경우 5-11.
    if ($dungeon == 'star')
    {
        checkClick($img, 870, 1275, 16173634, 15911229, '별탭');
        checkClick($img, 102, 1752, 3283976, 3612430, '하단스크롤');
        checkClick($img, 774, 1650, 13018698, 13280853, '5-11입장');
    }
    else if ($dungeon == 'hero')
    {
        checkClick($img, 870, 1605, 12486210, 12223548, '영웅탭');
        checkClick($img, 729, 1752, 3809296, 9862753, '상단스크롤');
        checkClick($img, 735, 1641, 13544026, 13741401, '6-1입장');
    }

    if ($isAutoSkill)
    {
        checkClick($img, 222, 1224, 1111, 1111, '자동스킬체크');
    }
    else
    {
        checkClick($img, 222, 1224, 15125107, 15125874, '스킬2번');
    }
    checkClick($img, 171, 900, 5711111, 5711111, '레벨30팝업닫기');
    checkClick($img, 141, 1479, 4351347, 4482420, '모험시작');
    checkClick($img, 753, 1218, 5907464, 5711112, '친구입장');
    checkClick($img, 132, 1272, 14070362, 14202463, '입장');
    checkClick($img, 213, 123, 11899450, 11899710, '자동전투');

    checkClick($img, 438, 900, 11365681, 11694134, '보물상자');
    checkClick($img, 126, 1281, 7026969, 6632722, '선택완료');
    checkClick($img, 123, 363, 14071386, 14267742, '다시하기');
    checkClick($img, 321, 657, 5908488, 5842697, '열쇠가 없을 시 취소');

    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

function checkClick($img, $x, $y, $color, $colorGenymotion, $ment = '')
{
    $rgb = imagecolorat($img, $x, $y);
    var_dump($ment . ' = ' . $rgb);
    if ($rgb == $color || $rgb == $colorGenymotion)
    {
        clickEvent($x, $y);
        clickEventForGenymotion($x, $y);
        echo $ment . ', x = ' . $x . ', y = ' . $y . ', color = ' . $color . PHP_EOL;
    }
}

function clickEvent($x, $y, $event = 'event0')
{
    sendEvent($event, 0x3, 0x39, 0x2eb9);
    sendEvent($event, 0x3, 0x35, $x);
    sendEvent($event, 0x3, 0x36, $y);
    sendEvent($event, 0x3, 0x3a, 0x35);
    sendEvent($event, 0x3, 0x32, 0x5);
    sendEvent($event, 0x3, 0x39, 0x0);
    sendEvent($event, 0x0, 0x0, 0x0);

    sendEvent($event, 0x3, 0x35, $x);
    sendEvent($event, 0x3, 0x36, $y);
    sendEvent($event, 0x0, 0x0, 0x0);

    sendEvent($event, 0x3, 0x39, 0xffffffff);
    sendEvent($event, 0x0, 0x0, 0x0);
}

function clickEventForGenymotion($x, $y, $event = 'event7')
{
    sendEvent($event, 0x1, 0x14a, 0x1);
    sendEvent($event, 0x3, 0x3a, 0x1);
    sendEvent($event, 0x3, 0x35, $x);
    sendEvent($event, 0x3, 0x36, $y);
    sendEvent($event, 0x0, 0x2, 0x0);
    sendEvent($event, 0x0, 0x0, 0x0);

    sendEvent($event, 0x1, 0x14a, 0x0);
    sendEvent($event, 0x3, 0x3a, 0x0);
    sendEvent($event, 0x3, 0x35, $x);
    sendEvent($event, 0x3, 0x36, $y);
    sendEvent($event, 0x0, 0x2, 0x0);
    sendEvent($event, 0x0, 0x0, 0x0);
}

function sendEvent($event, $one, $two, $three)
{
    exec("adb shell sendevent /dev/input/{$event} {$one} {$two} {$three}");
}
