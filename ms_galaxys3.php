<?php

$deviceName = '42f7a0068f588f43';

if (count($argv) <= 2)
{
    echo 'argv입력하삼. area stage ex) 2 9';
    exit;
}
$area = $argv[1];
$stage = $argv[2];
echo 'area = ' . $area . ', stage = ' . $stage . PHP_EOL;

$img = '';
$mode = 'story';

while(true)
{
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > ms_galaxys3_screenshot.png");
    $img = imagecreatefrompng('ms_galaxys3_screenshot.png');

    if ($mode == 'story')
    {
        checkClick(244, 981, 5253120, '스토리 게임시작');
        dragLeft();
    }
    else if ($mode == 'matjjang')
    {
        checkClick(384, 1012, 13083689, '맞짱뜨기');
        if (checkClick(84, 966, 13421772, '맞짱 상대찾기'))
        {
            // 맞짱 상대찾기 클릭.
        }
        else if (checkPixel(84, 966, 6710886, '맞짱상대찾기 dim'))
        {
            // 횟수 초과인 경우 story모드로 간다.
            $mode = 'story';
            clickEvent(51, 45);
            sleep(2);
        }
        checkSpecifyClick(442, 442, 167641346, 194, 792, '체력회복 팝업');
        checkClick(58, 704, 12111849, '준비');
        checkClick(102, 148, 10559269, '자동전투');
        checkClick(75, 617, 50595592, '맞짱 결과 확인');
    }
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

// 픽셀체크하는 함수.
function checkPixel($x, $y, $color, $ment = '')
{
    global $img;
    $rgb = imagecolorat($img, $x, $y);
    echo "{$ment}: x = {$x}, y = {$y}, pixel = {$rgb}" . PHP_EOL;
    if ($rgb == $color)
    {
        return true;
    }
    return false;
}

// 픽셀체크 후 클릭하는 함수.
function checkClick($x, $y, $color, $ment = '')
{
    if (checkPixel($x, $y, $color, $ment))
    {
        echo "touch {$ment}!" . PHP_EOL;
        clickEvent($x, $y);
    }
}

// 픽셀 체크 후 지정해서 클릭하는 함수.
function checkSpecifyClick($x, $y, $color, $clickX, $clickY, $ment)
{
    if (checkPixel($x, $y, $color, $ment))
    {
        echo "touch {$ment}!";
        clickEvent($clickX, $clickY);
    }
}

// 터치.
function clickEvent($x, $y, $event = 'event1')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x39, 0xa1);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x3, 0x30, 0xd);
    $exec .= sendEvent($event, 0x3, 0x31, 0x9);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
}

// 왼쪽 드래그
function dragLeft($event = 'event1')
{
    global $deviceName;
    $startX = 360;
    $startY = 250;
    $endX = 360;
    $endY = 500;
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x3, 0x39, 0xa1);
    $exec .= sendEvent($event, 0x3, 0x32, 0x9);
    $exec .= sendEvent($event, 0x3, 0x35, $startX);
    $exec .= sendEvent($event, 0x3, 0x36, $startY);
    $exec .= sendEvent($event, 0x3, 0x30, 0xc);
    $exec .= sendEvent($event, 0x3, 0x31, 0x8);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x3, 0x30, 0xe);
    $exec .= sendEvent($event, 0x3, 0x31, 0xc);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    /*
    $exec .= sendEvent($event, 0x3, 0x32, 0xa);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x3, 0x30, 0xf);
    $exec .= sendEvent($event, 0x3, 0x31, 0x7);
    $exec .= sendEvent($event, 0x3, 0x3c, 0x12);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    */
    $exec .= "\"";
    exec($exec);
}

// 오른쪽 드래그
function dragRight()
{

}

function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}

///dev/input/event1: EV_ABS       ABS_MT_TRACKING_ID   000000ff
///dev/input/event1: EV_ABS       ABS_MT_WIDTH_MAJOR   00000009
///dev/input/event1: EV_ABS       ABS_MT_POSITION_X    0000010a
///dev/input/event1: EV_ABS       ABS_MT_POSITION_Y    000001f2
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MAJOR   0000000c
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MINOR   00000008
///dev/input/event1: EV_ABS       003c                 00000019
///dev/input/event1: EV_SYN       SYN_REPORT           00000000
///dev/input/event1: EV_ABS       ABS_MT_WIDTH_MAJOR   0000000a
///dev/input/event1: EV_ABS       ABS_MT_POSITION_X    0000010c
///dev/input/event1: EV_ABS       ABS_MT_POSITION_Y    00000210
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MAJOR   0000000f
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MINOR   00000007
///dev/input/event1: EV_ABS       003c                 00000012
///dev/input/event1: EV_SYN       SYN_REPORT           00000000
///dev/input/event1: EV_ABS       ABS_MT_POSITION_X    0000010d
///dev/input/event1: EV_ABS       ABS_MT_POSITION_Y    00000239
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MINOR   00000008
///dev/input/event1: EV_ABS       003c                 00000010
///dev/input/event1: EV_SYN       SYN_REPORT           00000000
///dev/input/event1: EV_ABS       ABS_MT_POSITION_X    0000010b
///dev/input/event1: EV_ABS       ABS_MT_POSITION_Y    0000026b
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MAJOR   00000017
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MINOR   00000009
///dev/input/event1: EV_ABS       003c                 00000007
///dev/input/event1: EV_SYN       SYN_REPORT           00000000
///dev/input/event1: EV_ABS       ABS_MT_WIDTH_MAJOR   0000000b
///dev/input/event1: EV_ABS       ABS_MT_POSITION_X    0000010a
///dev/input/event1: EV_ABS       ABS_MT_POSITION_Y    0000027a
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MAJOR   00000009
///dev/input/event1: EV_ABS       ABS_MT_TOUCH_MINOR   00000007
///dev/input/event1: EV_ABS       003c                 00000040
///dev/input/event1: EV_SYN       SYN_REPORT           00000000
///dev/input/event1: EV_ABS       ABS_MT_TRACKING_ID   ffffffff
///dev/input/event1: EV_SYN       SYN_REPORT           00000000

///dev/input/event1: 0003 0039 000000ff
///dev/input/event1: 0003 0032 00000009
///dev/input/event1: 0003 0035 0000010a
///dev/input/event1: 0003 0036 000001f2
///dev/input/event1: 0003 0030 0000000c
///dev/input/event1: 0003 0031 00000008
///dev/input/event1: 0003 003c 00000019
///dev/input/event1: 0000 0000 00000000
///dev/input/event1: 0003 0032 0000000a
///dev/input/event1: 0003 0035 0000010c
///dev/input/event1: 0003 0036 00000210
///dev/input/event1: 0003 0030 0000000f
///dev/input/event1: 0003 0031 00000007
///dev/input/event1: 0003 003c 00000012
///dev/input/event1: 0000 0000 00000000
///dev/input/event1: 0003 0035 0000010d
///dev/input/event1: 0003 0036 00000239
///dev/input/event1: 0003 0031 00000008
///dev/input/event1: 0003 003c 00000010
///dev/input/event1: 0000 0000 00000000
///dev/input/event1: 0003 0035 0000010b
///dev/input/event1: 0003 0036 0000026b
///dev/input/event1: 0003 0030 00000017
///dev/input/event1: 0003 0031 00000009
///dev/input/event1: 0003 003c 00000007
///dev/input/event1: 0000 0000 00000000
///dev/input/event1: 0003 0032 0000000b
///dev/input/event1: 0003 0035 0000010a
///dev/input/event1: 0003 0036 0000027a
///dev/input/event1: 0003 0030 00000009
///dev/input/event1: 0003 0031 00000007
///dev/input/event1: 0003 003c 00000040
///dev/input/event1: 0000 0000 00000000
///dev/input/event1: 0003 0039 ffffffff
///dev/input/event1: 0000 0000 00000000