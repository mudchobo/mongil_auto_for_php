<?php

$deviceName = '192.168.56.101:5555';

if (count($argv) <= 2)
{
    echo 'argv입력하삼. mode area stage ex) story 2 9';
    exit;
}
$mode = $argv[1];
$area = $argv[2];
$stage = $argv[3];
echo 'mode = ' . $mode . ', area = ' . $area . ', stage = ' . $stage . PHP_EOL;

// 최초 실행하기 위함.
$isFirst = true;
$stopCount = 0;
$resultMatjjangStart = false;
$resultStoryStart = false;
$resultReady = false;
$resultRetry = false;
$img = '';

while(true)
{
    if ($isFirst)
    {
        exec("/Applications/Genymotion.app/Contents/MacOS/player --vm-name 'Google Nexus S - 4.1.1 - API 16 - 480x800_2' > /dev/null 2>/dev/null &");
        $isFirst = false;
        $stopCount = 0;
    }
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > ms_screenshot.png");
    $img = imagecreatefrompng('ms_screenshot.png');

    echo 'mode = ' . $mode . PHP_EOL;
    checkClick(61, 156, 16757872, 'icon game run'); // 아이콘 게임 실행
    checkClick(20, 747, 5151675, 'ad close');             // 광고 닫기
    checkSpecifyClick(263, 355, 16711680, 130, 392, 'receive ticket confirm'); // 식권받기 닫기.
    checkTwoSpecifyClick(49, 185, 0, 59, 171, 5876426, 49, 185, 'today not view');
    checkTwoSpecifyClick(61, 404, 0, 60, 596, 3367799, 38, 34, 'limited x');         // 무한세트 x버튼.
    checkSpecifyClick(300, 252, 2767692, 126, 499, 'limited x after confirm');   // 세트구매유도 후 확인.
    checkSpecifyClick(348, 124, 8893052, 30, 30, 'attendance');
    checkSpecifyClick(226, 186, 7771149, 150, 400, 'receive reward');                 // 보상받기
    checkTwoSpecifyClick(72, 102, 0, 429, 386, 13646908, 72, 102, 'auto skill story');            // 자동스킬 체크.
    checkTwoSpecifyClick(72, 102, 0, 444, 375, 3442080, 72, 102, 'auto skill matjjang');

    checkSpecifyClick(376, 106, 1188918, 30, 30, 'revenge close');          // 복수하기 닫기

    checkSpecifyClick(377, 126, 12296580, 284, 164, 'stress select');     // 스트레스타파 선택
    checkClick(116, 358, 15189504, 'stress gold select');                       // 스트레스 후 골드선택

    if ($mode == 'story')
    {
        $resultStoryStart = checkClick(150, 606, 5253120, 'story start');   // 스토리 게임시작
        if (checkPixel(428, 164, 1, 'story mode') && $area == 2)
        {
            if (checkClick(183, 600, 16488789, 'area 2') == false)
            {
                drag('left');
                sleep(2);
            }
        }
        // TODO 차후 다른 area추가.

        if ($stage == 9)
        {
            checkClick(199, 679, 15882331, 'stage 9');
        }
        checkClick(57, 607, 5374727, 'game start');
        checkClick(333, 686, 14869218, 'select friend');
        checkClick(59, 701, 23736066, 'real game start');
        checkTwoSpecifyClick(80, 337, 0, 71, 467, 6211060, 71, 467, 'item confirm');
        checkTwoSpecifyClick(62, 378, 0, 71, 399, 6211060, 74, 417, 'end confirm');
        $resultRetry = checkTwoSpecifyClick(42, 57, 0, 269, 415, 14005248, 42, 57, 'retry');
        // 다시하기가 dim인 경우 모드를 맞짱모드로 변경.
        if (checkPixel(48, 97, 2842487, 'retry dim') == true)
        {
            echo 'retry dim back!' . PHP_EOL;
            $mode = 'matjjang';
            clickEvent(43, 679);
        }
        // 하트부족
        if (checkPixel(356, 264, 8784132, 'not enough heart'))
        {
            echo 'heart!' . PHP_EOL;
            $mode = 'matjjang';
            clickEvent(60, 90);
            sleep(2);
            clickEvent(30, 30);
            sleep(2);
            clickEvent(30, 30);
            sleep(2);
            clickEvent(30, 30);
        }
    }
    else if ($mode == 'matjjang')
    {
        $resultMatjjangStart = checkClick(244, 679, 5253120, 'matjjang start');
        checkClick(48, 527, 15255296, 'find rival');
        // 라이벌이 딤인 경우 스토리모드로 전환.
        if (checkPixel(48, 527, 7627520, 'find rival dim') == true)
        {
            $stopCount = 0;
            $mode = 'story';
            sleep(5);
            clickEvent(30, 30);
        }

        if (checkTwoSpecifyClick(294, 317, 16777215, 136, 383, 0, 136, 383, 'impossible fight') == true)
        {
            sleep(2);
            clickEvent(30, 30);
        }
        checkClick(139, 501, 40818175, 'heal hp');
        $resultReady = checkClick(38, 429, 8228255, 'ready');
        checkTwoSpecifyClick(47, 387, 0, 395, 150, 10962182, 47, 387, 'end fight confirm');
    }

    if ($resultMatjjangStart == false && $resultStoryStart == false && $resultRetry == false && $resultReady == false)
    {
        $stopCount++;
        if ($stopCount == 150)
        {
            exec("adb -s {$deviceName} shell am force-stop com.pnixgames.muhan");
            sleep(5);
            $stopCount = 0;
        }
    }
    else
    {
        $resultMatjjangStart = false;
        $resultStoryStart = false;
        $resultRetry = false;
        $resultReady = false;
        $stopCount = 0;
    }
    echo 'stopCount = ' . $stopCount . PHP_EOL;
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

// 픽셀체크하는 함수.
function checkPixel($x, $y, $color, $ment = '')
{
    global $img;
    $rgb = imagecolorat($img, $x, $y);
    $rgbInfo = imagecolorsforindex($img, $rgb);
    echo "{$ment}: x = {$x}, y = {$y}, pixel = {$rgb}, " . json_encode($rgbInfo) . PHP_EOL;

    if ($rgb === $color)
    {
        return true;
    }
    return false;
}

// 픽셀체크 후 클릭하는 함수.
function checkClick($x, $y, $color, $ment = '')
{
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        echo "touch {$ment}!" . PHP_EOL;
        clickEvent($x, $y);
    }
    return $result;
}

function checkTwoSpecifyClick($x1, $y1, $color1, $x2, $y2, $color2, $clickX, $clickY, $ment)
{
    $result1 = checkPixel($x1, $y1, $color1, $ment);
    $result2 = checkPixel($x2, $y2, $color2, $ment);
    if ($result1 && $result2)
    {
        echo "touch {$ment}!" . PHP_EOL;
        clickEvent($clickX, $clickY);
    }
    return $result1 && $result2;
}

// 픽셀 체크 후 지정해서 클릭하는 함수.
function checkSpecifyClick($x, $y, $color, $clickX, $clickY, $ment)
{
    $result = checkPixel($x, $y, $color, $ment);
    if ($result)
    {
        echo "touch {$ment}!" . PHP_EOL;
        clickEvent($clickX, $clickY);
    }
    return $result;
}

// 터치.
function clickEvent($x, $y, $event = 'event7')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
}

// 왼쪽 드래그
function drag($type, $event = 'event7')
{
    global $deviceName;
    echo 'drag ' . $type . '!' . PHP_EOL;
    $startX = 360; $startY = 200; $endX = 360; $endY = 700;

    if ($type == 'left')
    {
        $startX = 360; $startY = 200; $endX = 360; $endY = 700;
    }
    else if ($type == 'right')
    {
        $startX = 360; $startY = 700; $endX = 360; $endY = 200;
    }

    $splitValue = 5;
    $intervalValue = abs($startY - $endY) / $splitValue;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= "\"";
    exec($exec);
    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX);
        if ($type == 'left')
        {
            $exec .= sendEvent($event, 0x3, 0x36, $startY + ($i * $intervalValue));
        }
        else if ($type == 'right')
        {
            $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValue));
        }
        $exec .= sendEvent($event, 0x0, 0x2, 0x0);
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
}

function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
