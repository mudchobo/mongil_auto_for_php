<?php

$mainX = 446; $mainY = 672; $mainColor = 5833991;

if (count($argv) <= 3)
{
    echo 'argv enter. isAutoSkill isNewArea area stage isTeam isInfinity deviceIp ex) 1 1 6 9 1 1';
    exit;
}
$isAutoSkill = $argv[1];
$isNew = $argv[2];
$area = $argv[3];
$stage = $argv[4];
$isTeam = $argv[5];
$isInfinity = $argv[6];
$deviceIp = (isset($argv[7]) && $argv[7] != '') ? $argv[7] : '101';
$deviceName = "192.168.56.{$deviceIp}:5555";
echo 'isAutoSkill = ' . $isAutoSkill . ', isNew = ' . $isNew . ', area = ' . $area . ', stage = ' . $stage . ', isTeam = ' . $isTeam . ', isInfinity = ' . $isInfinity . ', deviceName = ' . $deviceName . PHP_EOL;

// 최초 실행하기 위함.
$isFirst = true;
// 버튼이 클릭이 안될 때 재시작하기 위한 카운트.
$stopCount = 0;

$startResult = false;
$startTeamResult = false;
$startInfinityResult = false;

// adventure, team, infinity
$mode = 'adventure';
//$mode = 'team';
//$mode = 'infinity';

while(true)
{
    // 최초 genymotion 실행.
    if ($isFirst)
    {
        exec("adb -s {$deviceName} shell am start -n com.cjenm.monster/com.seed9.sr.SRActivity");
        $isFirst = false;
        $stopCount = 0;
    }
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen_{$deviceIp}.png");
    $img = imagecreatefrompng("screen_{$deviceIp}.png");

    echo 'mode = ' . $mode . PHP_EOL;

    checkSpecifyClick($img, 243, 624, 16767151, 122, 580, '서버선택');                   // 서버 선택
    //checkSpecifyClick($img, 243, 624, 16767151, 122, 580, '서버선택');                   // 서버 선택
    checkClick($img, 31, 688, 14211033,'close ad');            // 광고 팝업 닫기
    checkClick($img, 192, 279, 3487029,'close ad2');          // 광고 팝업 닫기2
    checkClick($img, 58, 402, 5776904, 'close notice popup'); // 공지팝업 닫기
    checkClick($img, 60, 405, 13544280,'close notice popup2');// 공지팝업 닫기2
    checkClick($img, 70, 395, 5975568,'touch screen');        // 화면을 터치해주세요
    checkClick($img, 87, 402, 5776904,'friend help close');   // 친구 도우미 이력닫기
    checkClick($img, 45, 324, 5776904,'buy popup close');     // 구매유도 팝업 닫기
    checkClick($img, 81, 400, 10516281, 'attendance popup');  // 출석부

    // 구매페이지로 갔을 때 예외처리.
    checkClick($img, 56, 404, 5776904, 'shop first popup close');         // 상점갔을 때 첫구매창 닫기
    checkClickBack($img, 51, 322, 15447160, 'manage team back');      // 팀관리갔을 때 뒤로가기.
    checkClickBack($img, 447, 174, 15715955, 'monster list back');   // 몬스터목록 뒤로가기
    checkClickBack($img, 343, 495, 4530944, 'shop back 2014-10-07');            // 상점 뒤로가기
    checkClick($img, 73, 400, 5711111, 'level30 popup close');         // 레벨30팝업닫기
    checkClick($img, 95, 50, 13939293, 'check auto fight');            // 자동전투 체크

    // 네트워크에러.
    checkSpecifyClick($img, 261, 329, 15916466, 141, 389, 'network error confirm'); // 네트워크에러 확인.
    // 자동스킬.
    if ($isAutoSkill && $mode == 'adventure')
    {
        if ($isAutoSkill == '2')
        {
            checkClick($img, 236, 611, 16104467, 'check Hero Potion');
        }
        checkClick($img, 381, 722, 9838760, 'check auto skill');      // 자동스킬 체크
    }

    // 팀대전 무한대전 공통.
    if ($mode == 'team' || $mode == 'infinity')
    {
        checkClickBack($img, 335, 641, 12823173, 'adventure start back');    // 모험시작화면에서 뒤로가기.
        checkClickBack($img, 394, 736, 14724973, 'map back 2014-10-24');                      // 신던전 지도 뒤로가기.
        checkClickBack($img, 153, 482, 5314305, 'old or new back');                // 구던전 신던전 선택부분 뒤로가기.
        checkClickBack($img, 375, 16, 14141076, 'adventure select stage back');    // 스테이지 선택 뒤로가기.
        checkClickBack($img, 281, 288, 5702050, 'adventure ready back');            // 모험준비 뒤로가기.
        checkSpecifyClick($img, $mainX, $mainY, $mainColor, 43, 702, 'enter fighting 2014-08-31');      // 대전입장.
    }
    // 모험 무한대전 공통.
    if ($mode == 'adventure' || $mode == 'infinity')
    {
        checkClickBack($img, 223, 461, 15510093, 'ready team back');      // 팀대전 준비 시 뒤로가기.
        if (checkClickBack($img, 54, 349, 5776904, 'start team back')){        // 팀대전 시작 시 뒤로가기.
            sleep(2);
            continue;
        }
    }
    // 모험 팀대전 공통.
    if ($mode == 'adventure' || $mode == 'team')
    {
        checkClickBack($img, 153, 467, 9729617, 'ready infinity back');        // 무한대전 준비 뒤로가기.
        checkClickBack($img, 228, 616, 1467043, 'start infinity back');     // 무한대전 시작 뒤로가기.
        checkClickBack($img, 228, 616, 1401507, 'start infinity back');     // 무한대전 시작 뒤로가기.
    }

    if ($mode == 'adventure')
    {
        checkClickBack($img, 78, 399, 6566929, 'enter fighting back');     // 대전 입장 화면 뒤로가기
        checkSpecifyClick($img, $mainX, $mainY, $mainColor, 46, 526, 'enter adventure');     // 모험입장

        if ($isNew == '0')
        {
            checkClick($img, 71, 227, 6698514, 'old enter');
            if ($area == '5')
            {
                if (checkClick($img, 382, 533, 5781528, 'star tab'))
                {
                    sleep(2);
                    continue;
                }
                if ($stage == '11')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 346, 730, 5711112, '5-11');    // 5-11
                }
                else if ($stage == '15')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 50, 730, 7422489, '5-15');     // 5-15
                    checkClick($img, 50, 730, 11372098, '5-15');    // 5-15
                    if (checkClick($img, 100, 455, 15917486, 'skill1')) {
                        sleep(1);
                    }
                    if (checkClick($img, 100, 550, 15917486, 'skill2')) {
                        sleep(1);
                    }
                    if (checkClick($img, 100, 650, 13088387, 'skill3')) {
                        sleep(1);
                    }
                    if (checkClick($img, 100, 745, 13088387, 'skill4')) {
                        sleep(1);
                    }
                }
            }
            else if ($area == '6')
            {
                if (checkClick($img, 382, 658, 5781528, 'hero tab'))
                {
                    sleep(2);
                    continue;
                }
                if ($stage == '1')
                {
                    if (upScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 327, 733, 14201179, '6-1');
                }
                else if ($stage == '5')
                {
                    if (upScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 49, 733, 13214548, '6-5');
                }
                else if ($stage == '8')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 353, 733, 10450489, '6-8');
                    checkClick($img, 353, 733, 8804904, '6-8');
                }
                else if ($stage == '9')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 273, 732, 12030281, '6-9');
                }
                else if ($stage == '10')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 203, 732, 5711112, '6-10');
                }
                else if ($stage == '11')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 126, 732, 7488282, '6-11');
                    checkClick($img, 126, 732, 7422746, '6-11');
                }
                else if ($stage == '12')
                {
                    if (downScroll($img))
                    {
                        continue;
                    }
                    checkClick($img, 57, 732, 12030281, '6-12');
                }
            }
        }
        else
        {
            checkClick($img, 71, 603, 6698514, 'new enter');
            // 현재 맵일 때에만 실행.
            if (checkPixel($img, 385, 758, 14724973, 'current map'))
            {
                // 난이도 설정 (isNew값이 1이면 쉬움, 2이면 어려움, 3이면 매우어려움)
                if ($isNew == '3' && !checkPixel($img, 40, 134, 12227658, 'very hard check'))
                {
                    echo 'very hard change!' . PHP_EOL;
                    clickEventForGenymotion(37, 135);
                    sleep(1);
                    clickEventForGenymotion(232, 143);
                    sleep(1);
                    continue;
                }
                else if ($isNew == '2' && !checkPixel($img, 44, 112, 12490829, 'hard check'))
                {
                    echo 'hard change!' . PHP_EOL;
                    clickEventForGenymotion(37, 135);
                    sleep(1);
                    clickEventForGenymotion(172, 143);
                    sleep(1);
                    continue;
                }
                else if ($isNew == '1' && !checkPixel($img, 31, 102, 10318647, 'normal check'))
                {
                    echo 'normal change!' . PHP_EOL;
                    clickEventForGenymotion(37, 135);
                    sleep(1);
                    clickEventForGenymotion(110, 143);
                    sleep(1);
                    continue;
                }
                //check_new_dungeon($img, $area, $stage);
            }
            // 맵화면에서 입장.
            checkClick($img, 164, 485, 5842697, 'map enter');
            // 자동스킬이 아닌 경우 스킬클릭.
            if ($isAutoSkill == '0')
            {
                if (checkClick($img, 100, 455, 15917486, 'skill1')) {
                    sleep(1);
                }
                if (checkClick($img, 100, 550, 15917486, 'skill2')) {
                    sleep(1);
                }
                if (checkClick($img, 100, 650, 13088387, 'skill3')) {
                    sleep(1);
                }
                if (checkClick($img, 100, 745, 13088387, 'skill4')) {
                    sleep(1);
                }
            }
        }

        // 모험시작 페이지인 경우
//        if (checkPixel($img, 337, 647, 8216906, 'adventure start page'))
//        {
//            // 모험용이라고 써 있으면 클릭.
//            if (checkPixel($img, 393, 95, 16702843, 'check adventure deck'))
//            {
//                clickEventForGenymotion(57, 656);
//            }
//            // 아닌경우 덱변경 클릭
//            else
//            {
//                clickEventForGenymotion(386, 64);
//            }
//        }
        checkClick($img, 39, 730, 13142355, '모험시작');
        checkClick($img, 61, 581, 5776904, '입장');        // 친구선택 입장
        checkClick($img, 174, 397, 16764930, 'select box');                // 보물상자 선택
        checkClick($img, 64, 524, 14332243, '선택완료.');            // 선택완료
        if (checkClick($img, 69, 118, 13937488, 'retry')) {                        // 다시하기
            $startResult = true;
        }
        checkSpecifyClick($img, 284, 284, 9073491, 135, 512, 'character full but enter');    // 케릭터가 꽉 차도 입장.
        checkSpecifyClick($img, 280, 291, 15916466, 135, 512, 'equipment full but enter');    // 장비가 꽉 차도 입장.
        checkSpecifyClick($img, 284, 287, 15916466, 135, 512, 'stone full but enter');			// 강화석이 꽉 차도 입장.
        $keyNotResult = checkSpecifyClick($img, 295, 207, 15916466, 136, 292, 'cancel not enough key'); // 열쇠가 없을 시 취소

        if ($keyNotResult)
        {
            if ($isTeam == '1' || $isTeam == '2')
            {
                $mode = 'team';
            }
        }
    }
    // 팀대전
    else if ($mode == 'team')
    {
        checkClick($img, 78, 399, 6566929, 'enter team');                           // 팀대전 입장.
        checkSpecifyClick($img, 223, 461, 15510093, 46, 704, 'ready team');      // 팀대전 준비.

        if ($isTeam == '2')
        {
            checkClick($img, 387, 473, 16312775, 'team defense item');     // 방어아이템 체크.
            checkClick($img, 387, 553, 16777215, 'team attack item');      // 공격아이템 체크.
        }
        // 팀대전시작 페이지인 경우
        if (checkPixel($img, 54, 349, 5776904, 'start team page'))
        {
            // 팀대용이라고 써 있으면 클릭.
            if (checkPixel($img, 384, 105, 16702843, 'check team deck'))
            {
                clickEventForGenymotion(46, 704);
                $startTeamResult = true;
            }
            // 아닌경우 덱변경 클릭
            else
            {
                clickEventForGenymotion(386, 64);
            }
        }

        checkClick($img, 100, 391, 8212257, 'continue team');                        // 대전 이어가기.
        $lackResult = checkClick($img, 136, 292, 12688207, 'lack'); // 팀대전 입장 모자를 때
        if ($lackResult == true)
        {
            if ($isInfinity)
            {
                $mode = 'infinity';
            }
            else
            {
                $mode = 'adventure';
            }
            sleep(2);
            clickEventForGenymotion(84, 534);
        }
    }
    // 무한대전.
    else if ($mode == 'infinity')
    {
        if ($isInfinity == '2')
        {
            checkClick($img, 381, 636, 7814488, 'check auto skill infinity');   // 무한대전 자동스킬 체크.
        }
        checkClick($img, 78, 145, 10121269, 'enter infinity');                           // 무한대전 입장.
        checkSpecifyClick($img, 153, 467, 9729617, 46, 704, 'ready infinity');        // 무한대전 준비.
        #$startInfinityResult1 = checkSpecifyClick($img, 228, 616, 1467043, 62, 667, 'start infinity');     // 무한대전 시작.
        #$startInfinityResult2 = checkSpecifyClick($img, 228, 616, 1401507, 62, 667, 'start infinity');     // 무한대전 시작.

        checkClick($img, 88, 398, 7751453, 'end infinity confirm');    // 무한대전 끝 확인.
        $lackResult = checkClick($img, 136, 292, 12688207, 'lack'); // 무한대전 입장 모자를 때
        if ($lackResult == true)
        {
            $mode = 'adventure';
            sleep(2);
            clickEventForGenymotion(84, 534);
        }
        // 모험용이라고 써 있으면 클릭.
        if (checkPixel($img, 393, 95, 16702843, 'check adventure deck'))
        {
            clickEventForGenymotion(57, 656);
            $startInfinityResult = true;
        }
        // 아닌경우 덱변경 클릭
        else
        {
            clickEventForGenymotion(386, 64);
        }
    }

    // 다시하기가 클릭되지 않거나 열쇠가 없는 경우 카운트. 200카운트면 재시작.
    if ($startResult == false && $startTeamResult == false && $startInfinityResult == false)
    {
        $stopCount++;
        if (($stopCount == 300 && $mode != 'infinity') || ($stopCount == 500 && $mode == 'infinity'))
        {
            echo 'stop!!! kill genymotion...';
            exec("adb -s {$deviceName} shell am force-stop com.cjenm.monster");
            sleep(10);
            $isFirst = true;
        }
    }
    else
    {
        $startResult = false;
        $startTeamResult = false;
        $startInfinityResult = false;
        $stopCount = 0;
    }
    echo 'stopCount = ' . $stopCount . PHP_EOL;
    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

function downScroll($img)
{
    return checkClick($img, 58, 780, 3612430, 'down scroll');
}

function upScroll($img)
{
    return
        checkClick($img, 334, 780, 3350286, 'up scroll');
}

function checkClick($img, $x, $y, $color, $ment = '')
{
    $result = checkPixel($img, $x, $y, $color, $ment);
    if ($result)
    {
        clickEventForGenymotion($x, $y);
        echo $ment . ' click!' . PHP_EOL;
    }
    return $result;
}

function checkSpecifyClick($img, $x, $y, $color, $clickX, $clickY, $ment)
{
    $result = checkPixel($img, $x, $y, $color, $ment);
    if ($result)
    {
        clickEventForGenymotion($clickX, $clickY);
        echo $ment . ' specify click!' . PHP_EOL;
    }
    return $result;
}

function checkClickBack($img, $x, $y, $color, $ment)
{
    $result = checkPixel($img, $x, $y, $color, $ment);
    if ($result)
    {
        clickEventForGenymotion(447, 30);
        echo $ment . ' back click!' . PHP_EOL;
    }
    return $result;
}

function checkPixel($img, $x, $y, $color, $ment)
{
    $rgb = imagecolorat($img, $x, $y);
    var_dump($ment . ' = ' . $rgb);
    if ($rgb == $color)
    {
        return true;
        clickEventForGenymotion(447, 30);
        echo $ment . ', x = ' . $x . ', y = ' . $y . ', color = ' . $color . PHP_EOL;
        return true;
    }
    return false;
}

function clickEventForGenymotion($x, $y, $event = 'event7')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
}

function drag($startX, $startY, $endX, $endY, $event = 'event7')
{
    global $deviceName;

    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= "\"";
    exec($exec);
    $splitValue = 30;

    $intervalValueX = ($startX - $endX) / $splitValue;
    $intervalValueY = ($startY - $endY) / $splitValue;

    for ($i = 0; $i < $splitValue; $i++)
    {
        $exec = "adb -s {$deviceName} shell \"";
        $exec .= sendEvent($event, 0x3, 0x35, $startX - ($i * $intervalValueX));
        $exec .= sendEvent($event, 0x3, 0x36, $startY - ($i * $intervalValueY));
        $exec .= sendEvent($event, 0x0, 0x2, 0x0);
        $exec .= sendEvent($event, 0x0, 0x0, 0x0);
        $exec .= "\"";
        exec($exec);
    }
    $exec = "adb -s {$deviceName} shell \"";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $endX);
    $exec .= sendEvent($event, 0x3, 0x36, $endY);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "\"";
    exec($exec);
    echo 'drag ' . $startX . ', ' . $startY . ', ' . $endX . ', ' . $endY . PHP_EOL;
}

function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
