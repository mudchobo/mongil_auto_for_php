<?php

$deviceName = '0183b8c912cab4f5';

if (count($argv) <= 2)
{
    echo 'argv입력하삼.';
    exit;
}
$isAutoSkill = $argv[1] == 'true' ? true : false;
$dungeon = $argv[2];
echo 'isAutoSkill = ' . $isAutoSkill . ', dungeon = ' . $dungeon . PHP_EOL;

while(true)
{
    // screen capture
    echo "screen capture..." . PHP_EOL;
    system("adb -s {$deviceName} shell screencap -p | perl -pe 's/\\x0D\\x0A/\\x0A/g' > screen.png");
    $img = imagecreatefrompng('screen.png');

    //checkClick($img, 136, 396, 5776904, '세션만료');
    checkClick($img, 31, 688, 6578281,'광고팝업닫기');
    checkClick($img, 58, 402, 5776904, '공지사항팝업닫기');
    checkClick($img, 60, 405, 13544280,'공지팝업');
    checkClick($img, 243, 945, 3815697,'화면을 터치해주세요');
    checkClick($img, 87, 402, 5776904,'친구도우미이력 확인');
    checkClick($img, 64, 310, 6500880,'구매팝업닫기');
    checkClick($img, 126, 397, 5776904, '출석부');

    // 구매페이지로 갔을 때 예외처리.
    checkClick($img, 60, 400, 6237709, '첫구매창 닫기.');
    checkClickBack($img, 1000, 180, 15129492, '상점 뒤로가기.');
    // exo던전.
    if ($dungeon == 'exo')
    {
        checkClick($img, 153, 745, 2043745, 'exo입장');
        checkClick($img, 78, 163, 11042878, 'exo입장2');
        checkClick($img, 67, 636, 8493472, 'exo모험시작');
    }
    else
    {
        checkClick($img, 100, 1130, 11919343, '모험입장');
    }

    // 별지역인 경우 5-11.
    if ($dungeon == 'star')
    {
        checkClick($img, 890, 1350, 15115850, '별탭');
        checkClick($img, 100, 1755, 3219472, '하단스크롤');
        checkClick($img, 780, 1640, 5383176, '5-11입장');
    }
    else if ($dungeon == '5-15')
    {
        checkClick($img, 890, 1350, 15115850, '별탭');
        checkClick($img, 100, 1755, 3219472, '하단스크롤');
        checkClick($img, 130, 1640, 10780986, '5-15입장');

        if (checkClick($img, 100, 455, 13088387, '스킬1번')) {
            sleep(1);
        }
        if (checkClick($img, 100, 550, 13088387, '스킬2번')) {
            sleep(1);
        }
        if (checkClick($img, 100, 650, 13088387, '스킬3번')) {
            sleep(1);
        }
        if (checkClick($img, 100, 745, 13088387, '스킬4번')) {
            sleep(1);
        }
    }
    else if ($dungeon == 'hero')
    {
        checkClick($img, 890, 1600, 8674601, '영웅탭');
        checkClick($img, 750, 1755, 3219472, '상단스크롤');
        checkClick($img, 740, 1650, 12951114, '6-1입장');
    }
    else if ($dungeon == '6-5')
    {
        checkClick($img, 8900, 1600, 8674601, '영웅탭');
        checkClick($img, 750, 1755, 3219472, '상단스크롤');
        checkClick($img, 49, 733, 13083218, '6-5입장');
    }
    else if ($dungeon == '6-7')
    {
        checkClick($img, 385, 687, 16436052, '영웅탭');
        checkClick($img, 58, 780, 3612430, '하단스크롤');
        checkClick($img, 273, 732, 14004829, '6-7입장');
    }

    if ($isAutoSkill)
    {
        checkClick($img, 820, 1700, 15719341, '자동스킬체크');
    }
    checkClick($img, 171, 900, 5711111, '레벨30팝업닫기');
    checkClick($img, 150, 1500, 7046796, '모험시작');
    checkClick($img, 130, 1300, 5974280, '입장');
    checkClick($img, 210, 115, 10776865, '자동전투');
    checkClick($img, 420, 900, 10247457, '보물상자');
    checkClick($img, 100, 1250, 14070362, '선택완료');
    checkClick($img, 120, 400, 13544018, '다시하기');
    checkClick($img, 300, 670, 5383176, '열쇠가 없을 시 취소');

    checkClickBack($img, 447, 174, 15715955, '몬스터도감 들어온 경우 뒤로가기.');

    echo 'sleep 2 second...' . PHP_EOL;
    sleep(2);
}

function checkClick($img, $x, $y, $color, $ment = '')
{
    $rgb = imagecolorat($img, $x, $y);
    var_dump($ment . ' = ' . $rgb);
    if ($rgb == $color)
    {
        clickEvent($x, $y);
        echo $ment . ', x = ' . $x . ', y = ' . $y . ', color = ' . $color . PHP_EOL;
        return true;
    }
    return false;
}

function checkClickBack($img, $x, $y, $color, $ment)
{
    $rgb = imagecolorat($img, $x, $y);
    var_dump($ment . ' = ' . $rgb);
    if ($rgb == $color)
    {
        clickEvent(1000, 75);
        echo $ment . ', x = ' . $x . ', y = ' . $y . ', color = ' . $color . PHP_EOL;
        return true;
    }
    return false;
}

function checkClickMulti($img, $x, $y, $color, $x2, $y2, $color2, $ment = '')
{
    $rgb = imagecolorat($img, $x, $y);
    $rgb2 = imagecolorat($img, $x2, $y2);
    var_dump($ment . ' = ' . $rgb . ', ' . $rgb2);
    if ($rgb == $color && $rgb == $color2)
    {
        clickEvent($x, $y);
        echo $ment . ', x = ' . $x . ', y = ' . $y . ', color = ' . $color . PHP_EOL;
        return true;
    }
    return false;
}

function clickEvent($x, $y, $event = 'event0')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell '";
    $exec .= sendEvent($event, 0x3, 0x39, 0x2eb9);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x35);
    $exec .= sendEvent($event, 0x3, 0x32, 0x5);
    $exec .= sendEvent($event, 0x3, 0x39, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x3, 0x39, 0xffffffff);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "'";
    exec($exec);
}

function clickEventForGenymotion($x, $y, $event = 'event7')
{
    global $deviceName;
    $exec = "adb -s {$deviceName} shell '";
    $exec .= sendEvent($event, 0x1, 0x14a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x1);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);

    $exec .= sendEvent($event, 0x1, 0x14a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x3a, 0x0);
    $exec .= sendEvent($event, 0x3, 0x35, $x);
    $exec .= sendEvent($event, 0x3, 0x36, $y);
    $exec .= sendEvent($event, 0x0, 0x2, 0x0);
    $exec .= sendEvent($event, 0x0, 0x0, 0x0);
    $exec .= "'";
    exec($exec);
}

function sendEvent($event, $one, $two, $three)
{
    return "sendevent /dev/input/{$event} {$one} {$two} {$three};";
}
